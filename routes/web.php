<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\BusinessTripController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::post('/add/employee', [EmployeeController::class, 'store']);
Route::get('/add/business-trip', [BusinessTripController::class, 'create']);
Route::post('/business-trip-saved', [BusinessTripController::class, 'store']);
Route::get('/choose/employee', [EmployeeController::class, 'index']);
Route::get('/show-business-trips/{employee}', [BusinessTripController::class, 'index']);

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return Inertia\Inertia::render('Dashboard');
})->name('dashboard');
