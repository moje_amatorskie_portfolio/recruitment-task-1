<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use App\Models\Employee;

class EmployeeTest extends TestCase
{
    // use DatabaseMigrations;
    //restores database every timen you //run test
    // use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        //
        $this->artisan('migrate:refresh --seed');
        $response = $this->post('/add/employee', [
            //array of values send to server by POST
        ]);
        // $this->seed();
    }

    public function testEmployeeCanBeAddedThroughTheFormByPost()
    {
        //check if logged in as some user
        // $this->actingAs(factory(User::class)->create());

        $response = $this->post('/add/employee', [
            //array of values send to server by POST
        ]);
        $response = $this->post('/add/employee', [
        ]);
        //asserts that in a fresh database there will be 3 customerss
        $this->assertCount(3, Employee::all());
    }
}
