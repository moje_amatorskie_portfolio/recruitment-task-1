<?php

namespace Tests\PK;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\BusinessTrip;
use App\Models\EmployeeBusinessTrip;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class DatesTest extends TestCase
{
    // use DatabaseMigrations;
    //restores database every timen you //run test
    // use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->artisan('migrate:refresh --seed');
        $response = $this->post('/add/employee', [
            //array of values send to server by POST
        ]);
    }

    public function testStartDateRequired()
    {
        $data = $this->formData();
        unset($data['start_date']);
        $response = $this->post('/business-trip-saved', $data);
        $response->assertSessionHasErrors('start_date');
        $response->assertStatus(302);
        //there should be no business trips in database
        $this->assertCount(0, BusinessTrip::all());
    }

    public function testStartDateCannotBeLaterThanEndDate()
    {
        $data = $this->formData();
        $data['end_date'] = '2020-08-01';
        $response = $this->post('/business-trip-saved', $data);
        $response->assertSessionHasErrors('end_date');
        $response->assertStatus(302);  //Found
        //there should be no business trips in database
        $this->assertCount(0, BusinessTrip::all());
    }

    public function testEmployeeCannotBeOnTwoTripsAtTheSameTime___SameDatePeriods()
    {
        $data = $this->formData();
        $response = $this->post('/business-trip-saved', $data);
        //insert exactly the same business trip for the same employee
        $response = $this->post('/business-trip-saved', $data);
        $response->assertSessionHasErrors('employee_id');
        $response->assertStatus(302);  //Found
        //there should be 1 business trip in database, because second shouldn't have been added
        $this->assertCount(1, BusinessTrip::all());
    }

    public function testEmployeeCannotBeOnTwoTripsAtTheSameTime___SecondDatePeriodOverLappingTheFirst()
    {
        $data = $this->formData();
        $response = $this->post('/business-trip-saved', $data);
        //insert  the second date period overlapping the first
        $data['start_date'] = '2020-09-03';
        $data['end_date'] = '2020-09-10';
        $response = $this->post('/business-trip-saved', $data);
        $response->assertSessionHasErrors('employee_id');
        $response->assertStatus(302);  //Found
        //there should be 1 business trip in database, which was added in earlier function
        $this->assertCount(1, BusinessTrip::all());
    }

    public function testEmployeeCannotBeOnTwoTripsAtTheSameTime___FirstDatePeriodOverLappingTheSecond()
    {
        $data = $this->formData();
        $response = $this->post('/business-trip-saved', $data);
        //insert  the second date period overlapping the first
        $data['start_date'] = '2020-08-30';
        $data['end_date'] = '2020-09-03';
        $response = $this->post('/business-trip-saved', $data);
        $response->assertSessionHasErrors('employee_id');
        $response->assertStatus(302);  //Found
        //there should be 1 business trip in database, which was added in earlier function
        $this->assertCount(1, BusinessTrip::all());
    }

    public function formData()
    {
        return [
                    'start_date' => '2020-09-01',
                    'start_hour' => '02',
                    'start_minute' => '10',
                    'end_date' => '2020-09-05',
                    'end_hour' => '23',
                    'end_minute' => '20',
                    'iso_country_code' => 'GB',
                    'employee_id' => '1',
               ];
    }
}
