<?php

namespace Tests\Feature;

use App\Models\Country;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\BusinessTrip;
use App\Models\EmployeeBusinessTrip;
use Illuminate\Http\Request;

use Illuminate\Foundation\Testing\DatabaseMigrations;

class FormTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->artisan('migrate:fresh --seed');
        $response = $this->post('/add/employee', [
        ]);
    }

    public function testShowAddBusinessTripForm()
    {
        $response = $this->get('/add/business-trip');
        $response->assertStatus(200);
    }

    public function testBusinessTripCanBeAddedThroughTheFormByPost()
    {
        //any exception errors will be displayed
        $this->withoutExceptionHandling();
        //check if logged in as some user
        // $this->actingAs(factory(User::class)->create());

        $response = $this->post('/business-trip-saved', [
            //array of values send to server by POST
            'start_date' => '2020-09-01',
            'start_hour' => '02',
            'start_minute' => '10',
            'end_date' => '2020-09-05',
            'end_hour' => '23',
            'end_minute' => '20',
            'iso_country_code' => 'GB',
            'employee_id' => '1',
            // '_token' => csrf_token(),
            // '_token' => 'VntTgjlkNikJpAHDxbgb1LmZUpKo0go8OLPr8Ifa', //this is
            //not needed because CSRF Middeware is automatically disabled when running tests
        ]);
        //asserts that in a fresh database there will be one business Trip
        $this->assertCount(1, BusinessTrip::all());
    }

    public function testShowBuisinessTripsForAUser()
    {
        $response = $this->get('/show-business-trips/1');
        // dd(777);
        $response->assertStatus(200);
    }
}
