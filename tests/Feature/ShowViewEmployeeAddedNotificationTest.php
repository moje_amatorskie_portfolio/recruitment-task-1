<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\BusinessTrip;
use App\Models\EmployeeBusinessTrip;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ShowViewEmployeeAddedNotificationTest extends TestCase
{
    // use DatabaseMigrations;
    //restores database every timen you run test
    // use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        //
        $this->artisan('migrate:refresh --seed');
        $response = $this->post('/add/employee', [
            //array of values send to server by POST
        ]);
    }

    public function testShowConfirmationViewAfterAddingEmployee()
    {
        $response = $this->post('/add/employee');
        $response->assertStatus(200);
    }
}
