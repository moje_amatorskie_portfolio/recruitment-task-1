<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ShowChooseEmployeeFormTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed();
        $response = $this->post('/add/employee', []);
    }

    public function testShowChooseEmployeeForm()
    {
        $response = $this->get('/choose/employee');
        $response->assertStatus(200);
    }
}
