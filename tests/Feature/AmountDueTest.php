<?php

namespace Tests\Feature;

use App\Models\Country;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\BusinessTrip;
use App\Models\EmployeeBusinessTrip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Illuminate\Foundation\Testing\DatabaseMigrations;

class AmountDueTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->artisan('migrate:fresh --seed');
        $response = $this->post('/add/employee', []);
        //a second employee
        $response = $this->post('/add/employee', []);
    }


    public function testAmountDueExcludingWeekendsLessThan_8_Days()
    {
        $response = $this->post('business-trip-saved', $this->formData());
        $amount_due = DB::table('business_trips')->latest()->first()->amount_due;
        $this->assertEquals($amount_due, 300);
    }

    public function testAmountDueIncludingWeekendMoreThan_8_Days()
    {
        $data = $this->formData();
        $data['employee_id'] = '2';
        $data['end_date'] = '2020-10-02';
        $response = $this->post('business-trip-saved', $data);
        $amount_due = DB::table('business_trips')->latest()->first()->amount_due;
        $this->assertEquals($amount_due, 3225);
    }

    public function formData()
    {
        return [
                    'start_date' => '2020-09-01',
                    'start_hour' => '14',
                    'start_minute' => '10',
                    'end_date' => '2020-09-04',
                    'end_hour' => '15',
                    'end_minute' => '20',
                    'iso_country_code' => 'GB',
                    'employee_id' => '1',
               ];
    }
}
