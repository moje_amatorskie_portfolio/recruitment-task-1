<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Repositories\BusinessTripRepository;
use \stdClass;

class CheckViewRenderTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->artisan('migrate:refresh --seed');
        $response = $this->post('/add/employee', [
            //array of values send to server by POST
        ]);
        $response = $this->post('/add/employee', [
            //array of values send to server by POST
        ]);
    }

    public function testViewAddEmployee()
    {
        $view = $this->view('employee_added', [
            'notice' => 'The new employee has been saved'
        ]);
        $view->assertSee('The new employee has been saved'); //only one string can go here
    }


    public function testViewShowBusinessTrips()
    {
        $businessTripRepository = new BusinessTripRepository();
        $stdObject = new stdClass();
        $stdObject->start_date = '2020-09-01 10:20';
        $stdObject->end_date = '2020-09-10 22:30';
        $stdObject->amount_due = '100.00';
        $stdObject->iso_country_code = 'DE';
        $allBusinessTripsForEmployee = collect([
            0 => $stdObject
        ]);
        //one can also convert array into std object by:
        //$stdSomeObject = (object) $someArray;
        $response = $this->get('/show-business-trips/1');
        $response->assertStatus(200);
        $view = $this->view('show_business_trips', [
            'allBusinessTripsForEmployee' =>  $allBusinessTripsForEmployee,
        ]);
        $view->assertSee('DE');
        $view->assertSee('2020-09-10 22:30');
        $view->assertSee('2020-09-10 22:30');
        $view->assertSee('100.00');
    }
}
