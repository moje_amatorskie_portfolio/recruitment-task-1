var isStartDateSet = false;
var isEndDateSet = false;
$( function() {
  $("#start_date").change( function ()  {
    isStartDateSet = true;
    if(isEndDateSet) {
      $("#form_button").removeClass('btn-secondary').addClass('btn-success');
      $("#form_button").attr("disabled", false);
    }
  });
  $("#end_date").change( () => {
    isEndDateSet = true;
    if(isStartDateSet) {
      $("#form_button").removeClass('btn-secondary').addClass('btn-success').attr('disabled', false);
    }
  });
  // $('#form_button').click( function()  {alert('fff'); //can't onClick disabled button
  //you can only use preventDefault and formSubmit
  //   if( $('#form_button').prop('disabled') ) {
  //     alert('iwef');
  //   }
  // });
} );
