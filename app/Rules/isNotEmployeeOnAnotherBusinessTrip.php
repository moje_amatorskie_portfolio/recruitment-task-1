<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Country;
use App\Models\Employee;
use App\Models\EmployeeBusinessTrip;
use App\Models\BusinessTrip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \DateTime;

class isNotEmployeeOnAnotherBusinessTrip implements Rule
{
    private $request;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $iso_country_code = $this->request->iso_country_code;
        $employee_id = $this->request->employee_id;
        $start_hour = $this->request->start_hour;
        $start_minute = $this->request->start_minute;
        $end_hour = $this->request->end_hour;
        $end_minute = $this->request->end_minute;
        $start_date = $this->request->start_date." ".$start_hour.":".$start_minute.":00";
        $start_date = new DateTime($start_date);
        $start_date_string = $start_date->format('yy-m-d H:i:s');
        $end_date = $this->request->end_date." ".$end_hour.":".$end_minute.":00";
        $end_date = new DateTime($end_date);
        $end_date_string = $end_date->format('yy-m-d H:i:s');

        $businessTrips = DB::table('employees')
            ->join('employee_business_trips', 'employees.id', '=', 'employee_business_trips.employee_id')
            ->join('business_trips', 'employee_business_trips.business_trip_id', '=', 'business_trips.id')
            ->select('business_trips.*')
            ->where('employee_business_trips.employee_id', '=', $employee_id)->get();
        $isNotEmployeeOnAnotherBusinessTrip = true;
        if ($businessTrips) {
            foreach ($businessTrips as $businessTrip) {
                if (
                    ($start_date >= new DateTime($businessTrip->start_date) && $start_date < new DateTime($businessTrip->end_date))
                    ||
                    ($end_date > new DateTime($businessTrip->start_date) && $end_date <= new DateTime($businessTrip->end_date))
              ) {
                    $isNotEmployeeOnAnotherBusinessTrip = false;
                }
            }
        }
        return $isNotEmployeeOnAnotherBusinessTrip;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "The employee can't be on two trips at the same time";
    }
}
