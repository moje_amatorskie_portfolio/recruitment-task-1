<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\BusinessTrip;
use App\Models\EmployeeBusinessTrip;
use App\Models\Country;
use App\Models\Employee;
use \DateTime;
use Illuminate\Support\Facades\Validator;
use App\Rules\isNotEmployeeOnAnotherBusinessTrip;
use Illuminate\Support\Facades\DB;
use App\Repositories\EmployeeRepository;
use App\Repositories\BusinessTripRepository;

class CountryRepository
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    //return collection of countries with daily expenses refund matching one of the following: 10 PLN, 50 PLN, 75 PLN
    public function listEligibleCountries()
    {
        $allCountries = Country::all();
        $eligibleCountries = $allCountries->whereIn('amount_due_per_day', [10,50,75]);
        $sortedEligibleCountries = $eligibleCountries->sortBy('iso_country_code');

        return $sortedEligibleCountries;
    }

    public function getCountryIdFromIso($iso_country_code)
    {
        $country_id = Country::where('iso_country_code', $iso_country_code)->first()->id;

        return $country_id;
    }

    public function getCountryAmountDuePerDayFromIso($iso_country_code)
    {
        $countryAmountDuePerDay = Country::where('iso_country_code', $iso_country_code)->firstOrFail()->amount_due_per_day;

        return $countryAmountDuePerDay;
    }

    public function store()
    {
        $employee = new Employee();
        $employee->save();

        return $employee->id;
    }
}
