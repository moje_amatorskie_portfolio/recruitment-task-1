<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Employee;

class EmployeeRepository
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        $employees = Employee::all();

        return $employees;
    }

    public function store()
    {
        $employee = new Employee();
        $employee->save();
        
        return $employee->id;
    }
}
