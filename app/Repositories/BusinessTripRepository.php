<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\BusinessTrip;
use App\Models\EmployeeBusinessTrip;
use App\Models\Country;
use App\Models\Employee;
use \DateTime;
use Illuminate\Support\Facades\Validator;
use App\Rules\isNotEmployeeOnAnotherBusinessTrip;
use Illuminate\Support\Facades\DB;
use \stdClass;

class BusinessTripRepository
{
    private $request;

    public function __construct(Request $request = null)
    {
        $this->request = $request;
    }

    public function index($employee_id)
    {
        $allBusinessTripsForEmployee = DB::table('employees')
          ->join('employee_business_trips', 'employees.id', '=', 'employee_business_trips.employee_id')
          ->join('business_trips', 'employee_business_trips.business_trip_id', '=', 'business_trips.id')
          ->join('countries', 'business_trips.country_id', '=', 'countries.id')
          ->select('business_trips.*', 'countries.iso_country_code')
          ->where('employee_business_trips.employee_id', '=', $employee_id)->get();
        return $allBusinessTripsForEmployee;
    }

    public function store()
    {
        $employee = new Employee();
        $employee->save();
        return $employee->id;
    }
}
