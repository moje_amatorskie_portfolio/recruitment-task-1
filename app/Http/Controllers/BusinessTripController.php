<?php

namespace App\Http\Controllers;

use App\Models\BusinessTrip;
use App\Models\EmployeeBusinessTrip;
use App\Models\Country;
use App\Models\Employee;
use Illuminate\Http\Request;
use \DateTime;
use Illuminate\Support\Facades\Validator;
use App\Rules\isNotEmployeeOnAnotherBusinessTrip;
use Illuminate\Support\Facades\DB;
use App\Repositories\BusinessTripRepository;
use App\Repositories\EmployeeRepository;
use App\Repositories\CountryRepository;

class BusinessTripController extends Controller
{
    private $businessTripRepository;
    private $employeeRepository;
    private $countryRepository;

    public function __construct(Request $request)
    {
        $this->businessTripRepository = new BusinessTripRepository($request);
        $this->employeeRepository = new EmployeeRepository($request);
        $this->countryRepository = new CountryRepository($request);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Employee $employee)
    {
        $notice = "";
        $alert_type = "";
        $allBusinessTripsForEmployee = $this->businessTripRepository->index($employee->id);
        if ($allBusinessTripsForEmployee->isEmpty()) {
            $allBusinessTripsForEmployee = null;
            $notice = "This employee has no business trips yet";
            $alert_type ="alert-danger";
        }

        return view('show_business_trips', compact('notice', 'alert_type', 'allBusinessTripsForEmployee'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $eligibleCountries = $this->countryRepository->listEligibleCountries();
        $employees = $this->employeeRepository->index();
        $hours = array();
        for ($i=0; $i<24; $i++) {
            if ($i<10) {
                $hours[] = '0'.$i;
            } else {
                $hours[] = $i;
            }
        }
        $minutes = array();
        for ($i=0; $i<60; $i++) {
            if ($i<10) {
                $minutes[] = '0'.$i;
            } else {
                $minutes[] = $i;
            }
        }

        return view('add_business_trip', compact('eligibleCountries', 'employees', 'hours', 'minutes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $notice = "The new business trip has been saved";
        $alert_type = 'alert-success';
        $iso_country_code = $request->iso_country_code;
        $country_id = $this->countryRepository->getCountryIdFromIso($iso_country_code);
        $employee_id = $request->employee_id;
        $start_hour = $request->start_hour;
        $start_minute = $request->start_minute;
        $end_hour = $request->end_hour;
        $end_minute = $request->end_minute;
        $start_date = $request->start_date." ".$start_hour.":".$start_minute.":00";
        $start_date = new DateTime($start_date);
        $start_date_string = $start_date->format('yy-m-d H:i:s');
        $end_date = $request->end_date." ".$end_hour.":".$end_minute.":00";
        $end_date = new DateTime($end_date);
        $end_date_string = $end_date->format('yy-m-d H:i:s');

        $validator = Validator::make($request->all(), [
           'start_date' => 'required',
           'end_date' => 'required|after_or_equal:start_date',
           'employee_id' => [new isNotEmployeeOnAnotherBusinessTrip($request)]
        ], [
          'end_date.after' => 'End date must be greater than start date'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $is_one_day_business_trip = false;
        $start_date_midnight = clone $start_date;
        $start_date_midnight->modify("+1 day");  //add one day because midnight is the beginning of a next day
        $start_date_midnight->setTime('00', '00');  //set hours and minutes to 00:00
        $end_date_midnight = clone $end_date;
        $end_date_midnight->setTime('00', '00');
        if ($start_date->format('yy-m-d') == $end_date->format('yy-m-d')) {  //that is, if business trip lasts only one day
            $is_one_day_business_trip = true;
            $difference_between_start_time_and_end_time = $start_date->diff($end_date);
        } else {
            $difference_between_start_time_and_midnight = $start_date->diff($start_date_midnight);
            $difference_between_end_time_and_midnight = $end_date_midnight->diff($end_date);
        }

        $number_of_weekend_days_per_business_trip = 0;
        $number_of_all_days_per_business_trip=0;
        $number_of_weekdays_per_business_trip=0;
        $amount_due_per_business_trip = 0;
        $is_weekend_day = false;
        $multiplier = 1; //1 if number of all days per business trip <=7, else multiplier = 2 so daily amount due doubles
        $country_amount_due_per_day = $this->countryRepository->getCountryAmountDuePerDayFromIso($iso_country_code);

        //count all days and weekend days
        $end_loop_date = (clone $end_date)->modify("+1 day");
        for ($i = clone $start_date; $i->format('yy-m-d') < $end_loop_date->format('yy-m-d'); $i = $i->modify("+1 day")) {
            $is_weekend_day = false;
            if ($i->format('D') == 'Sat' || $i->format('D') == 'Sun') {
                $number_of_weekend_days_per_business_trip++;
                $is_weekend_day = true;
            }
            $number_of_all_days_per_business_trip++;
            if ($number_of_all_days_per_business_trip > 7) {
                $multiplier = 2;
            }
            if (($i != $start_date) && ($i->format('yy-m-d') != $end_date->format('yy-m-d')) && (!$is_weekend_day)) {
                $amount_due_per_business_trip += $country_amount_due_per_day * $multiplier;
            } elseif ($i == $start_date && !$is_one_day_business_trip && !$is_weekend_day) {
                if (
                     ($difference_between_start_time_and_midnight->h +
                       $difference_between_start_time_and_midnight->days*24) >=8) {
                    $amount_due_per_business_trip += $country_amount_due_per_day * $multiplier;
                }
            } elseif ($i == $start_date && $is_one_day_business_trip && !$is_weekend_day) {
                if (
                     ($difference_between_start_time_and_end_time->h +
                     $difference_between_start_time_and_end_time->days*24) >= 8) {
                    $amount_due_per_business_trip += $country_amount_due_per_day * $multiplier;
                }
            } elseif (($i->format('yy-m-d') == $end_date->format('yy-m-d')) && !$is_one_day_business_trip && !$is_weekend_day) {
                if (
                    ($difference_between_end_time_and_midnight->h +
                     $difference_between_end_time_and_midnight->days*24)  >=8) {
                    $amount_due_per_business_trip += $country_amount_due_per_day * $multiplier;
                }
            }
        } //end for loop
        $number_of_weekdays_per_business_trip = $number_of_all_days_per_business_trip -
        $number_of_weekend_days_per_business_trip;

        //save business_trip
        $businessTrip = new BusinessTrip();
        $businessTrip->start_date = $start_date;
        $businessTrip->end_date = $end_date;
        $businessTrip->amount_due = $amount_due_per_business_trip;
        $businessTrip->country_id = $country_id;
        $businessTrip->save();
        //save employee and business trip in employee_business_trips table;
        $employeeBusinessTrip = new EmployeeBusinessTrip();
        $employeeBusinessTrip->employee_id = $employee_id;
        $employeeBusinessTrip->business_trip_id = $businessTrip->id;
        $employeeBusinessTrip->save();

        return view('business_trip_saved', compact('notice', 'alert_type'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BusinessTrip  $businessTrip
     * @return \Illuminate\Http\Response
     */
    public function show(BusinessTrip $businessTrip)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BusinessTrip  $businessTrip
     * @return \Illuminate\Http\Response
     */
    public function edit(BusinessTrip $businessTrip)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BusinessTrip  $businessTrip
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BusinessTrip $businessTrip)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BusinessTrip  $businessTrip
     * @return \Illuminate\Http\Response
     */
    public function destroy(BusinessTrip $businessTrip)
    {
        //
    }
}
