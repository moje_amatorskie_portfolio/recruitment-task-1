<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\Country;
use App\Models\BusinessTrip;
use App\Models\EmployeeBusinessTrip;
use App\Repositories\EmployeeRepository;
use App\Repositories\CountryRepository;

class EmployeeController extends Controller
{
    private $employeeRepository;

    public function __construct(Request $request)
    {
        $this->employeeRepository = new EmployeeRepository($request);
    }

    public function index()
    {
        $employees = $this->employeeRepository->index();
        //one can return custom status, eg. 200
        return response()->view('employee_list', compact('employees'), 200);
    }

    public function store(Request $request)
    {
        $employee_id = $this->employeeRepository->store();
        $notice = "The new employee nr ".$employee_id." has been added";
        $alert_type = 'alert-success';
        return view('employee_added', compact('notice', 'alert_type'));
    }
}
