<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeBusinessTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_business_trips', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger('employee_id')->nullable();
            $table->foreign('employee_id')
            ->references('id')
            ->on('employees');
            $table->unsignedBigInteger('business_trip_id')->nullable();
            $table->foreign('business_trip_id')
            ->references('id')
            ->on('business_trips');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_business_trips');
    }
}
