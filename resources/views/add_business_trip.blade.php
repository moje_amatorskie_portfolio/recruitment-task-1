@extends('layout');

@section('notice')
  <div class="text-secondary">You are adding a new business trip</div>
  @if (count($errors) > 0)
    <div class = "alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif
@endsection

@section('content')
<div class="content">
  <form method="POST" action="{{ url('/business-trip-saved') }}" autocomplete="off">
    @csrf
    <div class="text-left">
      <label for="start-date" class="font-50">Start date</label>
      <input class="text-center mt-5 text-lg date-picker" name="start_date" value="{{ old('start_date')??'Click to choose'}}" id="start_date" placeholder="Click to choose start date"/>
      <select class="font-50" style="width:130px;border:solid"  name="start_hour" placeholder="hour">
        @foreach ($hours as $hour)
          <option value="{{$hour}}" {{ (old('start_hour')?? '') == $hour ? 'selected' : ''}} >{{$hour}}</option>
        @endforeach
      </select>
      <span class="font-50">:</span>
      <select class="font-50" style="width:130px;border:solid" name="start_minute" placeholder="min">
        @foreach ($minutes as $minute)
          <option value="{{$minute}}" {{ (old('start_minute')?? '') == $minute ? 'selected' : ''}}>{{$minute}}</option>
        @endforeach
      </select>
    </div>

    <div class='text-left'>
      <label for="end-date" class="font-50">End date</label>
      <input class="text-center mt-5 text-lg date-picker" name="end_date" value="{{ old('end_date')??'Click to choose' }}" id="end_date" placeholder="Click to choose end date"/>
      <select class="font-50" style="width:130px;border:solid" name="end_hour" placeholder="hour">
        @foreach ($hours as $hour)
          <option value="{{$hour}}" {{ (old('end_hour')?? '') == $hour ? 'selected' : ''}}>{{$hour}}</option>
        @endforeach
      </select>
      <span class="font-50">:</span>
      <select class="font-50" style="width:130px;border:solid" name="end_minute" placeholder="min">
        @foreach ($minutes as $minute)
          <option value="{{$minute}}" {{ (old('end_minute')?? '') == $minute ? 'selected' : ''}}>{{$minute}}</option>
        @endforeach
      </select>
    </div>

    <div class="text-left mt-5">

      <label class="font-50" for="country">Country</label>
      <select class="font-50 ml-2" id="country" name="iso_country_code">
        @foreach ($eligibleCountries as $country)
          <option class="font-50" value="{{$country->iso_country_code}}" {{ (old('iso_country_code')??'') == $country->iso_country_code ? 'selected' : ''}}>{{$country->iso_country_code}}</option>
        @endforeach
      </select>



      <label  class="font-50 ml-5" for="employee">Employee id</label>
      <select class="font-50 ml-2" id="employee" name="employee_id">
        @foreach ($employees as $employee)
          <option class="font-50" value="{{$employee->id}}" {{ (old('employee_id')??'')==$employee->id ? 'selected' : '' }}>{{$employee->id}}</option>
        @endforeach
      </select>

    </div>

    <div class="mt-5 text-left">
      <button type="submit" class="btn-lg {{ count($errors)>0 ? 'btn-success' : 'btn-secondary' }}"  id="form_button" {{ count($errors)>0 ? '' : 'disabled' }}>Submit</button>
    </div>

  </form>
</div>

@endsection

@section('scripts')
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('datepicker.js') }}"></script>
<script src="{{ asset('formButton.js')}}"></script>
@endsection
