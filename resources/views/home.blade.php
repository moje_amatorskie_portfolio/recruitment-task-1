@extends('layout')

@section('content')
  <div class="content">
    <h1 class="text-left">Instructions from recruiter </h1>
    <div class="text-left">Create an application for calculating business trip refunds for employees.
      <ul>
        <li>
          business trips can only take place in countries where daily business trip refund is:<br>
          - PL: 10 PLN<br>
          - DE: 50 PLN<br>
          - GB: 75 PLN<br>
        </li>
        <li>
          business trip start date cannot be later than end date
        </li>
        <li>
          an employee can't be on two business trips at the same time
        </li>
        <li>
          refund is paid for every day but only if employee spends at least 8 hours on a given day
        </li>
        <li>
          Saturdays and Sundays are not paid
        </li>
        <li>
          if a business trip lasts longer than 7 days, then every day after that is paid double
        </li>
        <li>
          Endpoints:
        </li>
        <li>
          add employee to the system, no input data, employee id is returned in response(POST)
        </li>
        <li>
          add a business trip for an employee with input data:<br>
          - business trip start date and start time<br>
          - business trip end date and end time<br>
          - employee id<br>
          - country code in ISO 3166-1 alpha-2 format<br>
        </li>
        <li>
          show business trips for a chosen employee with data:<br>
          - start<br>
          - end<br>
          - country iso code<br>
          - amount due (PLN)
          - currency (PLN)
        </li>
      </ul>
  </div>
</div>
@endsection
