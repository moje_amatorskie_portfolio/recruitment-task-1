@extends('layout')

@section('content')
  <div class=''>
    @foreach ($employees as $employee)
      <div class='mt-2'>
        <form action="{{url('/show-business-trips')}}/{{$employee->id}}" method="GET">
          <span class='mr-5'>Employee no.{{ $employee->id }}</span><button type='submit' class='btn btn-primary'>Show business trips</button>
        </form>
      </div>
    @endforeach
  </div>
@endsection
