@extends('layout')

@section('content')
  @isset($allBusinessTripsForEmployee)
  <div class='content'>
    <table class='table-sm table-striped table-bordered table-hover'>
      <thead class='thead-dark'><th>Start date</th><th>End date</th><th>ISO Code</th><th>Amount due</th><th>Currency</th></thead>
    @foreach($allBusinessTripsForEmployee as $businessTrip)
    <tr>
      <td>{{$businessTrip->start_date}}</td>
      <td>{{$businessTrip->end_date}}</td>
      <td>{{$businessTrip->iso_country_code}}</td>
      <td>{{$businessTrip->amount_due}}</td>
      <td>PLN</td>
    </tr>
    @endforeach
   </table>
 </div>
  @endisset
@endsection

@section('notice')
  {{$notice??''}}
@endsection
